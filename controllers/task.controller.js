const taskModel = require('../models/task.model');

async function getAll(req, res) {
    const tasks = await taskModel.getAll();
    res.send(tasks); // .status(200);
}

async function get(req, res) {
    const id = req.params.id;

    const task = await taskModel.get(id);
    if (task) {
        res.send(task);
    } else {
        res.status(404).send(`La tarea con el id=${id} no existe`);
    }
}

module.exports = {
    getAll,
    get,
};