const express = require('express');
const taskController = require('./controllers/task.controller');

const app = express();

// define el puerto de escucha del server
const port = process.env.PORT || 3000;

/**
 *  GET /tareas -> getAll()
 *  POST /tareas -> insert()
 *  GET /tareas/:ID -> get(:id)
 */

app.get('/', (req, res) => {
    res.send('<h1>Hello world!</h1>');
});

app.get('/tareas', (req, res) => {
    taskController.getAll(req, res);
});

app.post('/tareas', (req, res) => {
    res.status(500).send('Method not implemented');
});

app.get('/tareas/:id', (req, res) => {
    taskController.get(req, res);
});

// abre el puerto y escucha solicitudes
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});
