createTareas = `
CREATE TABLE tareas (
    id_tarea int(11) NOT NULL,
    titulo varchar(200) NOT NULL,
    descripcion varchar(400) DEFAULT NULL,
    prioridad int(11) NOT NULL,
    finalizada tinyint(1) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
`;

insertTareas = `
INSERT INTO tareas (id_tarea, titulo, descripcion, prioridad, finalizada) VALUES
(1, 'Poder completar la clase', 'Venimos bien pero es larga', 5, 0),
(12, 'Hardcoding insert', 'Descrpcion', 1, 1),
(13, 'sdfgdgf', 'sdgfdsg', 1, 1),
(14, 'Hardcoding insert', 'Descrpcion', 1, 0),
(16, 'test2', 'test2', 2, 0),
(17, 'test3', 'test3', 2, 1),
(18, 'test4', 'test4', 3, 1),
(19, 'test4', 'test4', 3, 1),
(20, 'sdfsdf', 'sdfsdfs', 3, 1),
(21, 'asdasd', 'adsasdas', 4, 1);
`;

pkTareas = `ALTER TABLE tareas ADD PRIMARY KEY (id_tarea);`;

aiTareas = `ALTER TABLE tareas MODIFY id_tarea int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;`;

module.exports = {
    createTareas,
    insertTareas,
    pkTareas,
    aiTareas,
};