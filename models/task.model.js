// get the client
const mysql = require('mysql2/promise');
const migrations = require('./migrations');

// create the connection to database
const connectionPromise = mysql.createConnection({
  host: process.env.DB_HOST || 'localhost',
  user: process.env.DB_USER || 'root',
  password: process.env.DB_PASS || '',
  database: process.env.DB_SCHEMA || 'db_tareas',
});

async function migration() {
    const db = await connectionPromise;
    try {
        const [rows, fields] = await db.query('SELECT * FROM tareas');
    } catch(e) {
        db.execute(migrations.createTareas);
        db.execute(migrations.insertTareas);
        db.execute(migrations.pkTareas);
        db.execute(migrations.aiTareas);
    }
}

migration();

async function getAll() {
    const db = await connectionPromise;
    const [rows, fields] = await db.query('SELECT * FROM tareas');
    return rows;    
}

async function get(id) {
    const db = await connectionPromise;
    const [[row], fields] = await db.query('SELECT * FROM tareas WHERE id_tarea = ?', [id]);
    return row;    
}

module.exports = {
    getAll,
    get,
};